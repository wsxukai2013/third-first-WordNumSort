package com.xkhadoop.third.first;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;



public class Sort {
	public static class SimpleMapper extends
			Mapper<IntWritable, Text, RevertyKey, Text> {
		
		public void map(IntWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			RevertyKey newKey = new RevertyKey(key);
				context.write(newKey,value);
		}
	}

	public static class SimpleReducer extends
			Reducer<RevertyKey, Text, Text, IntWritable> {

		public void reduce(RevertyKey key, Iterable<Text> values,
				Context context) throws IOException, InterruptedException {
			for (Text val : values) {
				context.write(val, key.getKey());
			}
		
		}
	}

	public static class RevertyKey
		implements WritableComparable<RevertyKey>{
		private IntWritable key;
		public RevertyKey() {
			key=new IntWritable();
		}
		
		public RevertyKey(IntWritable key) {
			this.key=key;
		}

		public IntWritable getKey() {
			return key;
		}


		@Override
		public void readFields(DataInput in) throws IOException {
			key.readFields(in);
			
		}

		@Override
		public void write(DataOutput out) throws IOException {
			key.write(out);
		}

		@Override
		public int compareTo(RevertyKey o) {
			// TODO Auto-generated method stub
			return -key.compareTo(o.getKey());
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args)
				.getRemainingArgs();
		if (otherArgs.length < 2) {
			System.err.println("Usage: wordcount <in> [<in>...] <out>");
			System.exit(2);
		}
		@SuppressWarnings("deprecation")
		Job job = new Job(conf, "Sort");
		job.setJarByClass(Sort.class);
		job.setMapperClass(SimpleMapper.class);
		job.setReducerClass(SimpleReducer.class);
		
	    job.setMapOutputKeyClass(RevertyKey.class);
	    job.setMapOutputValueClass(Text.class);
	    job.setOutputKeyClass(Text.class);   
	    job.setOutputValueClass(IntWritable.class);
	    job.setInputFormatClass(SequenceFileInputFormat.class);
		
		for (int i = 0; i < otherArgs.length - 1; ++i) {
			FileInputFormat.addInputPath(job, new Path(otherArgs[i]));
		}
		FileOutputFormat.setOutputPath(job, new Path(
				otherArgs[otherArgs.length - 1]));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}

}
